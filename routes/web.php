<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route:: get('/', 'FrontentController@index');

Route:: get('/shop', 'FrontentController@shop');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/home', 'AdminController@index');
Route::get('admin','Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin','Admin\LoginController@Login');
Route::get('admin/logout','AdminController@logout')->name('admin.logout');


//========================Admin=Categories=Routes====================//
Route::get('admin/categories', 'admin\CategoriesController@index')->name('admin.categories');
Route::post('admin/categories-store', 'admin\CategoriesController@storeCat')->name('store.categories');
Route::get('admin/categories/edit/{cat_id}', 'admin\CategoriesController@edit');
Route::post('admin/categories-update', 'admin\CategoriesController@updateCat')->name('update.category');
Route::get('admin/categories/delete/{cat_id}', 'admin\CategoriesController@delete');
Route::get('admin/categories/inactive/{cat_id}', 'admin\CategoriesController@inactive');
Route::get('admin/categories/active/{cat_id}', 'admin\CategoriesController@active');

//===================Admin=Brand=Routes=============================//

Route::get('admin/brand', 'Admin\BrandController@index')->name('admin.brand');
Route::post('admin/brand-store', 'admin\BrandController@store')->name('store.brand');
Route::get('admin/brand/edit/{brand_id}', 'admin\BrandController@edit');
Route::post('admin/brand-update', 'admin\BrandController@update')->name('update.brand');
Route::get('admin/brand/delete/{brand_id}', 'admin\BrandController@delete');
 Route::get('admin/brand/inactive/{brand_id}', 'admin\BrandController@inactive');
 Route::get('admin/brand/active/{brand_id}', 'admin\BrandController@active');

 //=====================Admin=product=Routes========================//
 Route::get('admin/product/add', 'Admin\ProductController@addProduct')->name('add_product');
 Route::post('admin/product/storeProduct', 'Admin\ProductController@storeProduct')->name('store_product');
 Route::get('admin/product/manage', 'Admin\ProductController@manageProduct')->name('manage_product');
 Route::get('admin/product/edit/{product_id}', 'Admin\ProductController@editProduct');
 Route::post('admin/product/updateProduct', 'Admin\ProductController@updateProduct')->name('update_product');
 Route::post('admin/product/image-update', 'Admin\ProductController@updateImage')->name('update-image');
 Route::get('admin/product/delete/{product_id}', 'Admin\ProductController@destroy');
 Route::get('admin/product/inactive/{product_id}', 'Admin\ProductController@inactive');
 Route::get('admin/product/active/{product_id}', 'Admin\ProductController@active');

 //============================Admin-coupon=========================//
Route::get('admin/coupon/index', 'Admin\CouponController@index')->name('admin.coupon');
Route::post('admin/coupon/add', 'Admin\CouponController@storeCoupon')->name('store.coupon');
Route::get('admin/coupon/edit/{coupon_id}', 'Admin\CouponController@edit');
Route::post('admin/coupon/update','Admin\CouponController@update')->name('coupon_update');
Route::get('admin/coupon/delete/{coupon_id}', 'Admin\CouponController@delete');
Route::get('admin/coupon/inactive/{coupon_id}', 'Admin\CouponController@inactive');
Route::get('admin/coupon/active/{coupon_id}', 'Admin\CouponController@active');

//==============================frontend-routes============================//
    //--------------Add-to-card---------------------// 

Route::post('add/card/{product_id}', 'CardController@addToCard');
Route::get('cart', 'CardController@cartIndex');
Route::get('cart-delete/{cart_id}', 'CardController@destroy');
Route::post('quantity-update/{cart_id}', 'cardController@quantityUpdate');
Route::post('apply-coupon', 'cardController@applyCoupon');
Route::get('user-login', 'cardController@user_login');

//--------------------Add-to-wish-list-------------// 
Route::get('wish-list/{product_id}', 'WishListController@wish_list');
Route::get('wish-list-show', 'WishListController@wish_list_show');

