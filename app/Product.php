<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'brand_id', 'categorie_id', 'product_name', 'product_slug', 'product_code', 'product_quantity', 'short_description', 'long_description', 'price', 'image_one', 'image_two', 'image_three', 'status'
    ];

    public function categorie(){
        return $this->belongsTo(Categories::Class, 'categorie_id');
    }
}
