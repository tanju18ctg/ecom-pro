<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\WishList;


class WishListController extends Controller
{
    public function wish_list($product_id){
        if(Auth::check()){
            WishList::insert([
                'user_id'=> Auth::id(),
                'product_id'=> $product_id
        ]);
             return Redirect()->back()->with('cart-message', 'product added in Wish List');
        }
        else{
            return Redirect()->route('login')->with('login-error', 'You need to login first.');
        }
    }

    //----------------------wish-list-show--------------// 

    public function wish_list_show(){
        $wishlists = Wishlist::where('user_id', Auth::id())->latest()->get();
        return view('pages/wishlist', compact('wishlists'));
    }
}
