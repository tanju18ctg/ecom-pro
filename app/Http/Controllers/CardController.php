<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Coupon;
use Illuminate\Support\Facades\Session;

class CardController extends Controller
{
    
  public function addToCard(Request $request, $product_id){
       $check = Cart::where('product_id', $product_id)->where('user_ip', request()->ip())->first();
       if($check){
            Cart::where('product_id', $product_id)->increment('quantity');
            return Redirect()->back()->with('cart-message', 'product added in cart');
       }else{
        Cart::insert([
            'product_id'=>$product_id,
            'quantity'=>1,
            'price'=>$request->price,
            'user_ip'=>request()->ip()
        ]);
        return Redirect()->back()->with('cart-message', 'product added in cart');
       }
        
    }

    //--------------------cart-index----------------------// 

    public function cartIndex(){

      $carts = Cart::where('user_ip',request()->ip())->latest()->get();

      $total_price = Cart::all()->where('user_ip', request()->ip())->sum(
        function($total){
             return $total->price * $total->quantity;
        });

      return view('pages/cart', compact('carts', 'total_price'));
    }

    //-------------------card product delete-------------------// 

    public function destroy($cart_id){
        Cart::where('id', $cart_id)->where('user_ip', request()->ip())->delete();
        return Redirect()->back()->with('cart-delete', 'cart product deleted');
    }

    //-------------quantity-update-------------------//

    public function quantityUpdate(Request $request, $cart_id){
        Cart::where('id', $cart_id)->where('user_ip', request()->ip())->update([
          'quantity'=> $request->quantity
        ]);
        return Redirect()->back()->with('cart-update', 'product quantity updated');      
    }

    //----------------coupon-apply-----------------// 
    public function applyCoupon(Request $request){
        $check = Coupon::where('coupon_name', $request->coupon_name)->first();
        if($check){
          Session::put('coupon', [
            'coupon_name'=>$check->coupon_name,
            'discount'=>$check->discount,
          ]);
          return Redirect()->back()->with('cart-update', 'Successfuly coupon applied');
        }else{
          return Redirect()->back()->with('cart-delete', 'Coupon is invalid');
        }
    }

    //-------------------user-login------------//

    public function user_login(){
      return view('auth/login');
    }

    //-----------------------wish-list-----------//

    public function wish_list($product_id){


    }
}
