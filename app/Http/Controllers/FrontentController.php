<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Categories;
class FrontentController extends Controller
{
    public function index(){
    $products = Product::where('status', 1)->latest()->get();
    $ltstProduct = Product::where('status', 1)->latest()->limit(3)->get();
    $categories = Categories::where('status', 1)->latest()->get();
        return view('pages.index', compact('products', 'categories', 'ltstProduct'));
    }

    public function shop(){
        return view('pages.shop');
    }
    
}
