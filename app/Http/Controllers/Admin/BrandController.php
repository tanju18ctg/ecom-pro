<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use Carbon\Carbon;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
        $brands = Brand::latest()->get();
        return view('admin/brand/index', compact('brands'));
    }


    public function store(Request $request){
        // $request->validate([ 
        //     'categories_name'=> 'required|unique:categories, categories_name',
        // ]);              
        Brand::insert([
            'brand_name'=> $request->brand_name,
            'created_at'=> Carbon::now()
        ]);

       return Redirect()->back()->with('success', 'Brand Added');
}


//==============edit-function===============//

public function edit($brand_id){
   $brand = Brand::find($brand_id);
   return view('admin.brand.edit', compact('brand'));
}


//==============update-function==================//

    public function update(Request $request){
           $brand_id = $request->id;
        
           Brand::find($brand_id)->update([
            'brand_name'=> $request->brand_name,
            'updated_at'=> Carbon::now()
        ]);

        return Redirect()->route('admin.brand')->with('success', 'Brand Updated');
    }

    public function delete($brand_id){
        Brand::find($brand_id)->delete();
        return Redirect()->back()->with('success', 'Brand Deleted');
    }

    // public function inactive($brand_id){
    //     Brand::find($brand_id)->update(['status'=>0]);
    //     return Redirect()->route('admin.brand')->with('success', 'Brand Inactivated');
    // }

    // public function active($brand_id){
    //     Brand::find($brand_id)->update(['status'=>1]);
    //     return Redirect()->route('admin.brand')->with('success', 'Brand Activated');
    // }


    public function inactive($brand_id){
        Brand::find($brand_id)->update(['status'=>0]);
        return Redirect()->route('admin.brand')->with('success', 'Brand InActive');
    }


    public function active($brand_id){
        Brand::find($brand_id)->update(['status'=>1]);
        return Redirect()->route('admin.brand')->with('success', 'Brand Active');
    }
}
