<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Coupon;
use Carbon\Carbon;

class CouponController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //================== index-file------------------// 
    public function index(){
       $coupons =  Coupon::latest()->get();
       return view('admin/coupon/index', compact('coupons'));
    }

    //-----------------store-coupon------------------//
    
    public function storeCoupon(Request $request){
        $request->validate([
            'coupon_name'=> 'required'
        ]);
    
    $coupon_name = $request->coupon_name;
    $discount = $request->discount;
        
        Coupon::insert([
            'coupon_name'=>strtoupper($coupon_name),
            'discount'=>$discount,
            'created_at'=>Carbon::now()
        ]);

        return Redirect()->back()->with('success', 'Coupon Added');
    }

    //----------------edit-coupon----------------// 

    public function edit($coupon_id){
       $coupon =  Coupon::findOrFail($coupon_id);
       return view('admin/coupon/edit', compact('coupon'));
    }

    // ---------------update-coupon----------------// 

    public function update(Request $request){
            $coupon_id = $request->id;
            Coupon::findOrFail($coupon_id)->update([
                'coupon_name'=> $request->coupon_name,
                'updated_at'=>Carbon::now()
            ]);

         return Redirect()->route('admin.coupon')->with('catUpdated', 'Coupon Updated');
    }


    //--------------Delete-Coupon----------------// 

    public function delete($coupon_id){
        Coupon::findOrFail($coupon_id)->delete();
        return Redirect()->route('admin.coupon')->with('delete', 'Coupon Deleted');
    }

    //===========coupon active & inactive============= //

    public function inactive($coupon_id){
        Coupon::findOrFail($coupon_id)->update([
            'status'=>0
        ]);
        return Redirect()->route('admin.coupon')->with('catUpdated', 'Coupon Inactivated');
    }

    public function active($coupon_id){
        Coupon::findOrFail($coupon_id)->update([
            'status'=>1
        ]);
        return Redirect()->route('admin.coupon')->with('catUpdated', 'Coupon activated');
    }
}
