<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Categories;
use Carbon\Carbon;

class CategoriesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index(){
       $categories =  Categories::latest()->get();
        return view('admin.categories.index', compact('categories'));
    }

    public function storeCat(Request $request){
            // $request->validate([ 
            //     'categories_name'=> 'required|unique:categories, categories_name',
            // ]);              
            Categories::insert([
                'categorie_name'=> $request->categorie_name,
                'created_at'=> Carbon::now()
            ]);

           return Redirect()->back()->with('success', 'category Added');
    }

//    public function Edit($cat_id){
//       $categories = Categories::find($cat_id);
//       return view('admin.categories.edit', compact('categories'));
//    }

public function edit($cat_id){
    $categories = Categories::findOrFail($cat_id);
    return view('admin.categories.edit', compact('categories'));
}

public function updateCat(Request $request){
     $cat_id = $request->id;
     Categories::findOrFail($cat_id)->update([
     'categorie_name'=> $request->categorie_name,
     'updated_at'=> Carbon::now()
]);

    //return Redirect()->route('admin.categories')->with('catUpdated', 'category updated');
}

//----------------delete-functon--------------------//

    public function delete($cat_id){
        Categories::find($cat_id)->delete();
        return Redirect()->back()->with('delete', 'category Deleted');
    }

//-------------Active/inactive--------------------// 

    public function inactive($cat_id){
        Categories::find($cat_id)->update(['status'=>0]);
        return Redirect()->route('admin.categories')->with('catUpdated', 'Status InActive');
    }


    public function active($cat_id){
        Categories::find($cat_id)->update(['status'=>1]);
        return Redirect()->route('admin.categories')->with('catUpdated', 'Status Active');
    }

   
}
