<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\Categories;
use App\Brand;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Carbon\Carbon;


class productController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function addProduct(){
       $products = Product::latest()->get();
       $categories = Categories::latest()->get();
       $brands = Brand::latest()->get();

       return view('admin\product\add', compact('products', 'categories', 'brands'));
    }


//---------------------store-product--------------------//
    public function storeProduct(Request $request){
            $request->validate([
                'product_name'=>'required|max:255',
                'product_code'=>'required|max:255',
                'product_price'=>'required|max:255',
                'product_quantity'=>'required|max:255',
                'categorie_id'=>'required|max:255',
                'brand_id'=>'required|max:255',
                'short_description'=>'required',
                'long_description'=>'required',
                'image_one'=>'required|mimes:jpg,jpeg,png',
                'image_two'=>'required|mimes:jpg,jpeg,png',
                'image_three'=>'required|mimes:jpg,jpeg,png'      
            ],
        [
            'categorie_id.required'=>'Select your category',
            'brand_id.required'=>'Select your brand'
        ]);

                $image_one = $request->file('image_one');
                $name_gen = hexdec(uniqid()).'.'.$image_one->getClientOriginalExtension();
                Image::make($image_one)->resize(270,270)->save('frontend/img/product/upload/'.$name_gen);
                $image_url1 = 'frontend/img/product/upload/'.$name_gen;

                $image_two = $request->file('image_two');
                $name_gen = hexdec(uniqid()).'.'.$image_two->getClientOriginalExtension();
                Image::make($image_two)->resize(270,270)->save('frontend/img/product/upload/'.$name_gen);
                $image_url2 = 'frontend/img/product/upload/'.$name_gen;
//---------------------------insert-product-------------------//
                $image_three= $request->file('image_three');
                $name_gen = hexdec(uniqid()).'.'.$image_three->getClientOriginalExtension();
                Image::make($image_three)->resize(270,270)->save('frontend/img/product/upload/'.$name_gen);
                $image_url3 = 'frontend/img/product/upload/'.$name_gen;

        Product::insert([
            'categorie_id'=>$request->categorie_id,
            'brand_id'=>$request->brand_id,
            'product_name'=>$request->product_name,
            'product_code'=>$request->product_code,
            'product_slug'=>strtolower(str_replace(' ','-', $request->product_name)),
            'price'=>$request->product_price,
            'product_quantity'=>$request->product_quantity,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
            'image_one'=>$image_url1,
            'image_two'=>$image_url2,
            'image_three'=>$image_url3,
            'created_at'=>Carbon::now()
        ]);

        return Redirect()->back()->with('success', 'Product Added');
    }


   //==========================Manage Product=================//

   public function manageProduct(){
      $products=  Product::latest()->get();
       return view('admin/product/manage', compact('products'));
   }

   //=================edit-product===============//

   public function editProduct($id){
       $product =  Product::findOrFail($id);
       $categories = Categories::latest()->get();
       $brands = Brand::latest()->get();
       return view('admin/product/edit', compact('product', 'categories', 'brands'));
   }


   //=========================product-update===============// 

   public function updateProduct(Request $request){
        $product_id = $request->id;
        Product::findOrFail($product_id)->update([
            'categorie_id'=>$request->categorie_id,
            'brand_id'=>$request->brand_id,
            'product_name'=>$request->product_name,
            'product_code'=>$request->product_code,
            'product_slug'=>strtolower(str_replace(' ','-', $request->product_name)),
            'price'=>$request->product_price,
            'product_quantity'=>$request->product_quantity,
            'short_description'=>$request->short_description,
            'long_description'=>$request->long_description,
            'updated_at'=>Carbon::now()
        ]);
        return Redirect()->route('manage_product')->with('status', 'Product Data Updated');
   }

//=========================Image-update=======================// 


public function updateImage(Request $request){
    $product_id = $request->id;
    $image_one = $request->img1;
    $image_two = $request->img2;
    $image_three = $request->img3;

//---------------combined----------image--------------//

if($request->has('image_one') && $request->has('image_two') && $request->has('image_three')){
    unlink($image_one);
    unlink($image_two);
    unlink($image_three);
    
    //----image-one-insert-code---//
    $img_one = $request->file('image_one');
    $name_gen = hexdec(uniqid()).'.'.$img_one->getClientOriginalExtension();
    Image::make($img_one)->resize(270,270)->save('frontend/img/product/upload/'.$name_gen);
    $img_one_upload = 'frontend/img/product/upload/'.$name_gen;

    Product::find($product_id)->update([
        'image_one'=> $img_one_upload,
        'updated_at'=> Carbon::now()
    ]);

     //----image-two-insert-code---//
     $img_two = $request->file('image_two');
     $name_gen = hexdec(uniqid()).'.'.$img_two->getClientOriginalExtension();
     Image::make($img_two)->resize(270,270)->save('frontend/img/product/upload/'.$name_gen);
     $img_two_upload = 'frontend/img/product/upload/'.$name_gen;

     Product::find($product_id)->update([
         'image_two'=> $img_two_upload,
         'updated_at'=> Carbon::now()
     ]);


     //--------image-three-insert-code----------// 
     $img_three = $request->file('image_three');
     $name_gen = hexdec(uniqid()).'.'.$img_three->getClientOriginalExtension();
     Image::make($img_three)->resize(270,270)->save('frontend/img/product/upload/'.$name_gen);
     $img_three_upload = 'frontend/img/product/upload/'.$name_gen;

     Product::findOrFail($product_id)->update([ 
         'image_three'=> $img_three_upload,
         'updated_at'=> Carbon::now()
     ]);

    return Redirect()->route('manage_product');
    }

}


//==================product-delete=====================// 

public function destroy($product_id){
    $image = Product::findOrFail($product_id);
    $img_one = $image->image_one;
    $img_two = $image->image_two;
    $img_three = $image->image_three;

    unlink($img_one);
    unlink($img_two);
    unlink($img_three);

    Product::findOrFail($product_id)->delete();
    
    return Redirect()->back()->with('delete', 'product has been deleted');

}

//-------------active & inactive-------------// 

    public function inactive($product_id){
        Product::findOrFail($product_id)->update([
            'status'=> 0
        ]);
        return Redirect()->route('manage_product')->with('status', 'The product Inactivated');
    }

    public function active($product_id){
        Product::findOrFail($product_id)->update([
            'status'=>1
        ]);
        return Redirect()->route('manage_product')->with('status', 'The product Activated');
    }


}




