@extends('layouts.master_layouts')

@section('frontend_content')
     <!-- Breadcrumb Section Begin -->
     <section class="breadcrumb-section set-bg" data-setbg="{{asset('frontend')}}/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Shopping Cart</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>Shopping Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                @if(session('cart-delete'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{session('cart-delete')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                 @endif

        @if(session('cart-update'))
            <div class="alert alert-success alert-dismissible fade show" role="alert"> 
                <strong> {{session('cart-update')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true"> &times; </span>
                 </button>
            </div>
        @endif
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>cart</th>
                                    <th></th>
                                </tr>
                            </thead>
                       
                            <tbody>
                            @foreach($wishlists as $row)
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src="{{ asset($row->product->image_one) }}" alt="logo" style="height:120px; width:110px;">
                                        <h5>{{$row->product->product_name}}</h5>
                                    </td>
                                    <td class="shoping__cart__price">
                                        ${{$row->product->price}}
                                    </td>
                                    <td class="shoping__cart__total">
                                    <form action="{{ url('add/card/'.$row->id)}}" method="post">
                                        @csrf
                                        <input type="hidden" name="price" value="{{$row->product->price}}">
                                        <button type="submit" class="btn btn-sm btn-primary"> Add To Cart </button>
                                     </form>
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <a href="{{url('cart-delete/'.$row->id)}}">
                                            <span class="icon_close"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                      
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- Shoping Cart Section End -->
@endsection