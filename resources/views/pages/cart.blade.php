@extends('layouts.master_layouts')

@section('frontend_content')
     <!-- Breadcrumb Section Begin -->
     <section class="breadcrumb-section set-bg" data-setbg="{{asset('frontend')}}/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Shopping Cart</h2>
                        <div class="breadcrumb__option">
                            <a href="./index.html">Home</a>
                            <span>Shopping Cart</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                @if(session('cart-delete'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{session('cart-delete')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                 @endif

        @if(session('cart-update'))
            <div class="alert alert-success alert-dismissible fade show" role="alert"> 
                <strong> {{session('cart-update')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true"> &times; </span>
                 </button>
            </div>
        @endif
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr>
                                    <th class="shoping__product">Products</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <th></th>
                                </tr>
                            </thead>
                       
                            <tbody>
                            @foreach($carts as $cart)
                                <tr>
                                    <td class="shoping__cart__item">
                                        <img src="{{ asset($cart->product->image_one) }}" alt="logo" style="height:120px; width:110px;">
                                        <h5>{{$cart->product->product_name}}</h5>
                                    </td>
                                    <td class="shoping__cart__price">
                                        ${{$cart->price}}
                                    </td>
                                    <td class="shoping__cart__quantity">
                                        <div class="quantity">
                                        <form action="{{ url('quantity-update/'.$cart->id) }}" method="post">
                                                @csrf
                                            <div class="pro-qty">
                                                <input type="text" name="quantity" value="{{$cart->quantity}}" min="1">
                                            </div>
                                                <button type="submit" class="btn btn-primary">update</button>
                                        </form>
                                        </div>
                                    </td>
                                    <td class="shoping__cart__total">
                                        ${{$cart->price * $cart->quantity}}
                                    </td>
                                    <td class="shoping__cart__item__close">
                                        <a href="{{url('cart-delete/'.$cart->id)}}">
                                            <span class="icon_close"></span>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                      
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a href="{{ url('/') }}" class="primary-btn cart-btn">CONTINUE SHOPPING</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        @if(Session::has('coupon'))
                        @else
                        <div class="shoping__discount">
                            <h5>Discount Codes</h5>
                            <form action="{{ url('apply-coupon') }}" method="post">
                             @csrf
                                <input type="text" name="coupon_name" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        <ul>
                            <li>Subtotal <span>${{$total_price}}</span></li>
                            <li>Discount <span>{{ session()->get('coupon')['discount']}}% 
                            ( {{ $discount = $total_price * session()->get('coupon')['discount'] / 100}})</span></li>
                            <li>Grand Total <span>${{$total_price - $discount}}</span></li>
                        </ul>
                        <a href="#" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->
@endsection