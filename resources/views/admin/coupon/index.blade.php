@extends('admin.admin_master')

@section('coupon')
    active
@endsection

@section('admin_content')
<div class="sl-mainpanel">
    <nav class="breadcrumb sl-breadcrumb">
      <a class="breadcrumb-item" href="index.html">Admin</a>
      <span class="breadcrumb-item active">Coupon</span>
    </nav>

    <div class="sl-pagebody">
      <div class="row row-sm">
          <div class="col-md-8"> 
          <div class="sl-page-title">
          <h5>Coupon List</h5>
          <p>DataTables is a plug-in for the jQuery Javascript library.</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Coupon Details</h6>
          <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
      <!-------------session-message-updated---------> 
      @if(session('catUpdated'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('catUpdated')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif

    <!-------------session-message-deleted---------> 
      @if(session('delete'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{session('delete')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Sl No</th>
                  <th class="wd-15p">coupon name</th>
                  <th class="wd-15p">Discount</th>
                  <th class="wd-20p">Status</th>
                  <th class="wd-25p">Action</th>
                </tr>
              </thead>
              <tbody>
               
               @foreach ($coupons as $row)
                <tr>
                  <td>{{$row->id}}</td>
                  <td>{{ $row->coupon_name }}</td>
                  <td>{{ $row->discount }}%</td>
                  <td>
                    @if($row->status == 1)
                      <span class="badge badge-success"> Active </span>
                      @else
                      <span class="badge badge-danger"> Inactive</span>
                    @endif
                    </td>
                    
                  <td>
                    <a href="{{ url('admin/coupon/edit/'.$row->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                    <a href="{{ url('admin/coupon/delete/'.$row->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    @if($row->status == 1)
                      <a href="{{ url('admin/coupon/inactive/'.$row->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-down"></i></a>
                    @else
                      <a href="{{ url('admin/coupon/active/'.$row->id) }}" class="btn btn-success btn-sm"><i class="fa fa-arrow-up"></i></a>
                    @endif
                  </td>   
                @endforeach
              </tbody>

            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
          </div>


        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Add Coupon
                </div>

                <div class="card-body">
                <!---------session-message-added-------->
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('success')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      @endif
                  
                    <form action="{{ route('store.coupon') }}" method="POST">
                        @csrf
                        <div class="form-group">
                          <label for="exampleInputEmail1">Coupon Name</label>
                          <input type="text" name="coupon_name" class="form-control @error('categories_name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Coupon Name">

                          @error('coupon_name')
                            <span class="text-danger">{{$message}}</span>
                          @enderror
                        </div>

                        <div class="form-group">
                          <label for="exampleInputEmail1">Coupon Discount</label>
                          <input type="text" name="discount" class="form-control @error('categories_name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter discount">

                          @error('discount')
                            <span class="text-danger">{{$message}}</span>
                          @enderror
                        </div>
                        <button type="submit" class="btn btn-primary">Add Coupon</button>
                      </form>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection