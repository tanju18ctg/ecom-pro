@extends('admin.admin_master')

@section('admin_content')
<div class="sl-mainpanel">
    <nav class="breadcrumb sl-breadcrumb">
      <a class="breadcrumb-item" href="index.html">Starlight</a>
      <span class="breadcrumb-item active">Dashboard</span>
    </nav>


        <div class="col-md-4">
            <div class="card">
                <div class="card-header">edit Category
                </div>

                <div class="card-body">
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('success')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      @endif

                    <form action="{{ url('update.category/'.$categories->id) }}" method="POST">
                        @csrf
                        <input type="hidden" value="{{$categories->id}}" name="id">

                        <div class="form-group">
                          <label for="exampleInputEmail1">Update Category</label>
                          <input type="text" name="categorie_name" class="form-control @error('categories_name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $categories->categorie_name}}">

                          @error('category_name')
                            <span class="text-danger">{{$message}}</span>
                          @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">update Category</button>

                      </form>
                </div>
            </div>
        </div>
    </div>

</div>


@endsection

