@extends('admin.admin_master')

@section('category')
    active
@endsection

@section('admin_content')
<div class="sl-mainpanel">
    <nav class="breadcrumb sl-breadcrumb">
      <a class="breadcrumb-item" href="index.html">Starlight</a>
      <span class="breadcrumb-item active">Dashboard</span>
    </nav>

    <div class="sl-pagebody">
      <div class="row row-sm">
          <div class="col-md-8"> 
          <div class="sl-page-title">
          <h5>Data Table</h5>
          <p>DataTables is a plug-in for the jQuery Javascript library.</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Basic Responsive DataTable</h6>
          <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
      <!-------------session-message-updated---------> 
      @if(session('catUpdated'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('catUpdated')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif

        @if(session('catUpdated'))
            <div class="alert alert-success alert-dismissible fade show" role="alert"> 
                <strong> {{session('catUpdated')}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="close">
                    <span aria-hidden="true"> &times; </span>
                 </button>
            </div>
        @endif

    <!-------------session-message-deleted---------> 
      @if(session('delete'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{session('delete')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Sl No</th>
                  <th class="wd-15p">Category name</th>
                  <th class="wd-20p">Status</th>
                  <th class="wd-25p">Action</th>
                </tr>
              </thead>
              <tbody>
               
               @foreach ($categories as $category)
                <tr>
                  <td>{{$category->id}}</td>
                  <td>{{ $category->categorie_name }}</td>
                  <td>
                    @if($category->status == 1)
                      <span class="badge badge-success"> Active </span>
                      @else
                      <span class="badge badge-danger"> Inactive</span>
                    @endif
                    </td>
                    
                  <td>
                    <a href="{{ url('admin/categories/edit/'.$category->id) }}" class="btn btn-primary btn-sm">Edit</a>
                    <a href="{{ url('admin/categories/delete/'.$category->id) }}" class="btn btn-danger btn-sm">Delete</a>
                    @if($category->status == 1)
                      <a href="{{ url('admin/categories/inactive/'.$category->id) }}" class="btn btn-danger btn-sm">inactive</a>
                    @else
                      <a href="{{ url('admin/categories/active/'.$category->id) }}" class="btn btn-success btn-sm">active</a>
                    @endif
                  </td>   
                @endforeach
              </tbody>

            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
          </div>


        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Add Category
                </div>

                <div class="card-body">
                <!---------session-message-added-------->
                    @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('success')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      @endif
                  
                    <form action="{{ route('store.categories') }}" method="POST">
                        @csrf
                        <div class="form-group">
                          <label for="exampleInputEmail1">Add Category</label>
                          <input type="text" name="categorie_name" class="form-control @error('categories_name') is-invalid @enderror" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Category">

                          @error('category_name')
                            <span class="text-danger">{{$message}}</span>
                          @enderror
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                      </form>
                </div>
            </div>
        </div>
    </div>

</div>




@endsection