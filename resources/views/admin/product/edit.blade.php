@extends('admin.admin_master')

@section('admin_content')
<div class="sl-mainpanel">
    <nav class="breadcrumb sl-breadcrumb">
      <a class="breadcrumb-item" href="index.html">Starlight</a>
      <span class="breadcrumb-item active">Dashboard</span>
    </nav>

    <div class="sl-pagebody">
    <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Update Product</h6>

          @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('success')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif

      <p class="mg-b-20 mg-sm-b-30">A form with a label on top of each form control.</p>
       <form action="{{ route('update_product') }}" method="post">
         @csrf
         <input type="hidden" name="id" value="{{$product->id}}">
          <div class="form-layout">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">product_name:<span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="product_name" value="{{ $product->product_name}}" placeholder="Enter product name">
                  @error('product_name')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
              </div><!-- col-4 -->

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">product_code: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="product_code" value="{{ $product->product_code}}" placeholder="product code">
                  @error('product_code')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
              </div><!-- col-4 -->

              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Product_Price: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="product_price" value="{{ $product->price}}" placeholder="product price">
                  @error('product_price')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
              </div><!-- col-4 -->

              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">product_quantity: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="product_quantity" placeholder="product quantity" value="{{$product->product_quantity}}">
                  @error('product_quantity')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
              </div><!-- col-8 -->

              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Category: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" name="categorie_id" data-placeholder="Choose categorie">
                    <option label="Choose category"></option>
                    @foreach($categories as $category)
                      <option value="{{$category->id}}" {{$category->id == $product->categorie_id ?'selected' : ""}}>{{$category->categorie_name}}</option>
                    @endforeach
                  </select>
                  @error('categorie_id')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
              </div><!-- col-4 -->

      <div class="col-lg-4"> 
            <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Brand: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" name="brand_id" data-placeholder="Choose brand">
                    <option label="Choose Brand"></option>
                    @foreach($brands as $brand)
                      <option value="{{$brand->id}}" {{$brand->id == $product->brand_id ? 'selected':''}}>{{$brand->brand_name}}</option>
                    @endforeach
                  </select>
                  @error('brand_id')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
            </div>
      </div>

      <div class="col-lg-12">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">short_description: <span class="tx-danger">*</span></label>
                  <textarea name="short_description" id="summernote" cols="30" rows="10"> {{$product->short_description}} </textarea> 
                  @error('short_description')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
       </div><!-- col-8 -->

       <div class="col-lg-12">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Long_Description: <span class="tx-danger">*</span></label>
                  <textarea name="long_description" id="summernote2" cols="30" rows="10"> {{$product->short_description}} </textarea>
                  @error('long_description')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
                </div>
       </div><!-- col-8 -->
       <div class="form-layout-footer">
              <button class="btn btn-info mg-r-5" type="submit">Update Data</button>
          </div><!-- form-layout-footer -->
      
       </form>
    
       <form action="{{ route('update-image') }}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" value="{{$product->id}}">
          <input type="hidden" name="img1" value="{{$product->image_one}}">
          <input type="hidden" name="img2" value="{{$product->image_two}}">
          <input type="hidden" name="img3" value="{{$product->image_three}}">
      <div class="row row-sm mt-5">
        <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Image Thumbnail <span class="tx-danger">*</span></label>
                    <img src="{{asset($product->image_one)}}" alt="product" width="120px" height="120px">
                 </div>
        </div><!-- col-8 -->

        <div class="col-lg-4">
                    <div class="form-group mg-b-10-force">
                        <label class="form-control-label">Image Two <span class="tx-danger">*</span></label>
                        <img src="{{asset($product->image_two)}}" alt="product" width="120px" height="120px">
                    </div>
                </div><!-- col-8 -->

        <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Image Three <span class="tx-danger">*</span></label>
                    <img src="{{asset($product->image_three)}}" alt="product" width="120px" height="120px">
                </div>
        </div><!-- col-8 -->

        <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                    <label class="form-control-label">Image Thumbnail <span class="tx-danger">*</span></label>
                    <input class="form-control" type="file" name="image_one">
                    @error('image_one')
                        <strong class="text-danger"> {{$message}} </strong>
                    @enderror
                </div>
        </div>

        <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Image_two <span class="tx-danger">*</span></label>
                  <input class="form-control" type="file" name="image_two">
                  @error('image_two')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
        </div>
        </div><!-- col-8 -->

        <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Image_three <span class="tx-danger">*</span></label>
                  <input class="form-control" type="file" name="image_three">
                  @error('image_three')
                    <strong class="text-danger"> {{$message}} </strong>
                  @enderror
            </div>
        </div><!-- col-8 -->
   

        </div><!-- col-8 -->
        
          <div class="form-layout-footer">
              <button class="btn btn-info mg-r-5" type="submit">Update Product</button>
          </div><!-- form-layout-footer -->
       </form>
          </div><!-- form-layout -->
          
        </div><!-- card -->
       
      </div>
    </div>

</div>

@endsection