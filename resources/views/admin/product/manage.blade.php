@extends('admin.admin_master')

@section('product')
    active show-sub
@endsection

@section('manage_product')
    active
@endsection

@section('admin_content')
<div class="sl-mainpanel">
    <nav class="breadcrumb sl-breadcrumb">
      <a class="breadcrumb-item" href="index.html">Admin</a>
      <span class="breadcrumb-item active">Brand</span>
    </nav>

    <div class="sl-pagebody">
      <div class="row row-sm">
          <div class="col-md-12"> 
          <div class="sl-page-title">
          <h5>Brand List</h5>
          <p>DataTables is a plug-in for the jQuery Javascript library.</p>
        </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Manage Products</h6>
          <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
      <!-------------session-message-updated---------> 
      @if(session('catUpdated'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('catUpdated')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif

        @if(session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('status')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif

    <!-------------session-message-deleted---------> 
      @if(session('delete'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>{{session('delete')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
        @endif
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Image</th>
                  <th class="wd-15p">Product Name</th>
                  <th class="wd-15p">Category</th>
                  <th class="wd-15p">Product Quantity</th>
                  <th class="wd-10p">status</th>
                  <th class="wd-25p">Action</th>
                </tr>
              </thead>
              <tbody>
               
               @foreach ($products as $row)
                <tr>
                  <td class="align-baseline"><img src="{{asset($row->image_one)}}" alt="image" width="100px" height="auto"></td>
                  <td class="align-baseline">{{ $row->product_name }}</td>
                  <td class="align-baseline"> {{ $row->categorie['categorie_name'] }} </td>
                  <td class="align-baseline">{{ $row->product_quantity }}</td>
                   <td class="align-baseline">
                    @if($row->status == 1)
                      <span class="badge badge-success"> Active </span>
                      @else
                      <span class="badge badge-danger"> Inactive</span>
                    @endif
                    </td>                 
                  <td class="align-baseline">
                    <a href="{{ url('admin/product/edit/'.$row->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                    <a href="{{ url('admin/product/delete/'.$row->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                    @if($row->status == 1)
                      <a href="{{ url('admin/product/inactive/'.$row->id) }}" class="btn btn-danger btn-sm"><i class="fa fa-arrow-down"></i></a>
                    @else
                      <a href="{{ url('admin/product/active/'.$row->id) }}" class="btn btn-success btn-sm"><i class="fa fa-arrow-up"></i></a>
                    @endif
                  </td>   
                @endforeach
              </tbody>

            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
          </div>

    </div>

</div>




@endsection